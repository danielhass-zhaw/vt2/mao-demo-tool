from datetime import date
import time
import os

with open('data/control-{date}.csv'.format(date=int(time.time())), 'w') as output:
    output.write("{}".format(int(time.time())))
    output.write(f", DEMO_TOOL_ENV: {os.getenv('DEMO_TOOL_ENV', 'not set')}")
